## Measuring Topical Similarity on Reddit

The topical structure of a conversation gives insight on the way interaction is organised at a more global level than utterance sequences. It enablles us to understand how coherence is maintained throughout a dialogue. Creating a topical similarity measure which would give a similarity score to two pieces of interaction in terms of topic could enable us to analyse this structure. We train an unsupervised measure based on the \textit{Next Sentence Prediction} BERT model on Reddit conversations. The structure of Reddit provides us different coherence levels between pairs of messages which allows us to train our model with a marginal ranking loss rather than with numerical values. This measure enables us to find locally coherent pieces of interaction in our dataset but also to measure the variability in terms of topic throughout a conversation.

## Installation

## Install Instructions
The following installation instruction are designed for command line on Unix systems. Refer to the instructions for Git and Anaconda on your exploitation system for the corresponding instructions.

### Cloning the Repository
Clone the repository to your local machine, using the following command:

```bash
git clone git@gitlab.inria.fr:adecker/reddittopicsimilarity.git
```
This will download the repository.

### Installing the Dependencies

Create a python virtual environment (for example `redditvenv`) and install the dependencies, using the following commands:

```bash
python -m venv redditvenv
source redditvenv/bin/activate
pip3 install transformers
pip3 install pytorch
pip3 install treelib pickle
pip3 install pandas
pip3 install sklearn
pip3 install tqdm
```

## Usage

### Scrape a Subreddit

To scrape Reddit data you need to request credentials by creating an app on https://www.reddit.com/prefs/apps (you also need a Reddit account to do this). You can select the `script` type. Then enter those credentials in the file mlcode/scrape_reddit.py.
- client_id: the sequence of characters under the type of yout app (`personal use script` if you chose the script type);
- client_secret: the sequence of characters next to `secret`;
- user_agent: your Reddit identifier.
You can then use the code by running `python3 mlcode/scrape_reddit.py -s TheNameOfYourSubreddit`. The name of your subreddit appears in the url of the home page of this subreddit, for example `AskReddit` for `https://www.reddit.com/r/AskReddit/` and `todayilearned` for `https://www.reddit.com/r/todayilearned/`.
This code will create a json file in the folder `json_files` with the 100 top posts and their associated data as well as a list with all the comments for each post. If some threads take too much time to be retrieved, the url will appear in a file `{subreddit_name}_failing_urls.csv` after the execution of the code.

Then you can use the code mlcode/format_trees.py to transform your json files into trees and serialise them. This code will be called the first time the data is used if no tree version exists yet so this step is not mandatory.

### Train and Evaluate a Model
To train a model, run `python mlcode/train.py -e <number of epochs> -s <subreddit name> -n <number of triples> -x <experience name>` (ex: python3 mlcode/train.py -e 5 -s Coronavirus -n 10000 -x test). The generated model will be saved in the folder `model`.
To train a model, run `python mlcode/test.py -s <subreddit name> -x <experience name>` (ex: python3 mlcode/test.py -s Coronavirus -x test).

### Run some Analysis
To compute the topical clusters in a subreddit and generate the related graphs and statistics `python3 thread_similarity.py -s <subreddit name> -x <experience name>` (ex: python3 thread_similarity.py -s Coronavirus -x test). The generated files will be saved in the folder `stats` and `clusters`.
To compute the multi-level topical clusters in a subreddit and generate the related statistics `python3 thread_similarity_multilevel.py -s <subreddit name> -x <experience name>` (ex: python3 thread_similarity_multilevel.py -s Coronavirus -x test). The generated files will be saved in the folder `stats`.

## Files and Folders

### `json_files`
*DO NOT SHARE UNLESS REDDIT ALLOWS US TO.*
This folder contains the content scraped from the following 10 subreddits: [AITAH](https://www.reddit.com/r/AITAH/), [Art](https://www.reddit.com/r/Art/), [AskReddit](https://www.reddit.com/r/AskReddit/), [boardgames](https://www.reddit.com/r/boardgames/), [Coronavirus](https://www.reddit.com/r/Coronavirus/), [gaming](https://www.reddit.com/r/gaming/), [OnePiece](https://www.reddit.com/r/OnePiece/), [PoliticalDiscussion](https://www.reddit.com/r/PoliticalDiscussion/), [sports](https://www.reddit.com/r/sports/), [todayilearned](https://www.reddit.com/r/todayilearned/).

### `trees_files`
*DO NOT SHARE UNLESS REDDIT ALLOWS US TO.*
This folder contains our actual data, the scraped subreddits saved as python Trees using the library [Treelib](https://pypi.org/project/treelib/). The `.json` files are there for user readability but require a specific parsing to be used with *Treelib*. The `.tree`files can be directly read with *Pickle*.

### `mlcode`
This folder contains the code to to scrape the content from a given subreddit. Please check [Reddit's Data API Terms](https://www.redditinc.com/policies/data-api-terms) to know what you are allowed to do or not with their data. It also contains the code to train and evaluate the topic similarity models.
- `scrape_reddit.py`: Code to collect a certain subreddit, creates a json file;
- `format_trees.py`: Code to transform the json files into trees from the Treelib library and serialise them, creates a humanly readable json file and a serialised .tree file;
- `data_trees.py`: Code to load the datasets for the model;
- `model.py`: Our topical similarity measurement model;
- `train.py`: Code to train a model;
- `test.py`: Code to evaluate a model;
- `cross_evaluate.py`: Code to evaluate a model on a given dataset (potentially different from the dataset the model was trained on);
- `dis_from_root.py`: Code to compute the distance from the root for each comment in a thread; 
- `dis_subsequent`: Code to compute the distance between each pair of subsequent messages;
- `thread_similarity`: Code to compute the topical clusters in a subreddit and generate the related graphs and statistics;
- `thread_similarity_multilevel`: Code to compute the multi-level topical clusters in a subreddit and generate the related statistics.

## Support
If you want further information and/or guidance regarding this project, please feel free to contact me at amandine *dot* decker *at* loria *dot* fr.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## License
[![License: CC BY 4.0](https://licensebuttons.net/l/by/4.0/80x15.png)](https://creativecommons.org/licenses/by/4.0/)

## Project status
This project is currently ongoing so some files may be frequently updated.
