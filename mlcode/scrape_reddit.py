import praw
import time
import json
import os
import csv

# Set up your Reddit API credentials. You'll need to create a Reddit app to get these.
reddit = praw.Reddit(
    client_id='',
    client_secret='',
    user_agent='',
)

# Function to extract the original post and top 3 comments from a submission
def extract_post_and_comments_from_submission(submission):
    dict_tree = {
        'text': '\n\n'.join([submission.title, submission.selftext]).strip(),
        'url': submission.url,
        'id': submission.id,
        'upvote': submission.ups,
        'downvote': submission.downs,
        'author': None if submission.author is None else submission.author.name,
        'children': []
    }

    submission.comments.replace_more(limit=None)

    all_comments = submission.comments.list()
    print(f"\tNumber of comments: {len(all_comments)}")

    for comment in all_comments:
        comm_dict = {
            'id': comment.id,
            'parent_id': comment.parent_id,
            'text': comment.body,
            'upvote': comment.ups,
            'downvote': comment.downs,
            'author': None if comment.author is None else comment.author.name,
        }
        dict_tree['children'].append(comm_dict)

    print(f"\tActual number of scraped comments: {len(dict_tree['children'])}")
    
    return dict_tree

if __name__ == '__main__':
    subreddit_name = "sports" # "politics", "PoliticalDiscussion", "AITAH", "One Piece" # done: "Coronavirus", "gaming"
    if os.path.isfile(f"json_files/{subreddit_name}.json"):
        with open(f"json_files/{subreddit_name}.json", 'r') as openfile:
            # Reading from json file
            all_posts = json.load(openfile)
            print(f"Loaded: {len(all_posts)}")
            existing_urls = [post['url'] for post in all_posts]
    else:
        all_posts = []
        existing_urls = []
    
    if os.path.isfile(f"{subreddit_name}_failing_urls.csv"):
        with open(f"{subreddit_name}_failing_urls.csv", 'r') as f:
            failing_urls = f.read().splitlines()
    else:
        failing_urls = []
    
    all_submissions = list(reddit.subreddit(subreddit_name).top(time_filter="all"))#.hot())
    all_valid_submissions = [submission for submission in all_submissions if submission.url not in failing_urls and submission.url not in existing_urls]

    try:
        print(f"{len(all_valid_submissions)}/{len(all_submissions)-len(failing_urls)}")
        for submission in all_valid_submissions:
            print(submission.url)
            extracted, trials = False, 0
            while not extracted and trials < 2:
                try:
                    t = extract_post_and_comments_from_submission(submission)
                    extracted = True
                    existing_urls.append(submission.url)
                    all_posts.append(t)
                    print(f"Scraped: {len(all_posts)}/{len(all_submissions)-len(failing_urls)}")
                except Exception as e:
                    print(f"Error ({trials+1}/2): {e}")
                    time.sleep(5)
                finally:
                    trials += 1
            if not extracted:
                failing_urls.append(submission.url)
            print("Wait...")
            time.sleep(3)
            
        all_submissions = list(reddit.subreddit(subreddit_name).top(time_filter="all"))
        all_valid_submissions = [submission for submission in all_submissions if submission.url not in failing_urls and submission.url not in existing_urls]
    except Exception as e:
        print(f"Error: {e}")
        time.sleep(8)
        all_submissions = list(reddit.subreddit(subreddit_name).top(time_filter="all"))
        all_valid_submissions = [submission for submission in all_submissions if submission.url not in failing_urls and submission.url not in existing_urls]
        print('\tContinue')
    finally:
        # Serializing json
        json_object = json.dumps(all_posts, indent=4)
        # Writing to sample.json
        with open(f"json_files/{subreddit_name}.json", "w") as outfile:
            outfile.write(json_object)
        # Save failed urls
        print('\n'*3, failing_urls)
        with open(f"{subreddit_name}_failing_urls.csv", 'w') as f:
            f.write('\n'.join(failing_urls))
