from torch.utils.data import Dataset
from sklearn.model_selection import train_test_split
from format_trees import json_to_tree
import pickle
import pandas as pd
import os
import random as rd
import itertools
import numpy as np
from functools import reduce
import argparse
from tqdm import tqdm

MODES = ["train", "test"]
TRAIN_TEST_SPLIT_TYPES = ["thread", "vertical"]
TRIPLE_TYPES = ["unique", "all"]

np.random.seed(0)
rd.seed(0)

def parse_args():
    parser = argparse.ArgumentParser(usage='python data_trees.py -s boardgames -m train -n 100000')
    parser.add_argument('-s', '--subreddit_name', help='name of the used subreddit', type=str, default='boardgames')
    parser.add_argument('-m', '--mode', help='train or test mode', type=str, default='train')
    parser.add_argument('-i', '--train_test_split_type', help='spliting method for the train/test split', type=str, default='thread')
    parser.add_argument('-j', '--triples_type', help='all triples or one per comment', type=str, default='unique')
    parser.add_argument('-n', '--num_triples', help='number of triples in the dataset', type=int, default=100000)
    args = parser.parse_args()
    return args

def get_father(node_id, tree):
    return tree.parent(node_id)

def get_sons(node_id, tree):
    return tree.children(node_id)

def get_brothers(node_id, tree):
    return tree.siblings(node_id)

def get_uncles(node_id, tree):
    return tree.siblings(tree.parent(node_id).identifier)

def get_nephews(node_id, tree):
    all_brothers = get_brothers(node_id, tree)
    return sum([tree.children(bro.identifier) for bro in all_brothers], [])


class RedditDataset(Dataset):
    def __init__(self, subreddit_name, mode, train_test_split_type, triples_type, num_triples, tokenizer, max_length=128):
        self.data = self.__dataBuilder__(subreddit_name, mode, train_test_split_type, triples_type, num_triples)
        self.tokenizer = tokenizer
        self.max_length = max_length

    def __len__(self):
        return len(self.data)
    
    def __getitem__(self, idx):
        parent_son, brothers, uncle_nephew = self.data[idx]

        # construct and encode pos/neg pairs in NSP manner
        pos_pair = self.tokenizer(parent_son[0], parent_son[1], padding='max_length', max_length = self.max_length, truncation=True, return_tensors='pt')
        neg1_pair = self.tokenizer(brothers[0], brothers[1], padding='max_length', max_length = self.max_length, truncation=True, return_tensors='pt')
        neg2_pair = self.tokenizer(uncle_nephew[0], uncle_nephew[1], padding='max_length', max_length = self.max_length, truncation=True, return_tensors='pt')

        return [pos_pair, neg1_pair, neg2_pair]

    def __dataBuilder__(self, subreddit_name, mode, train_test_split_type, triples_type, num_triples):
        '''Returns triples of pairs of texts of the form:
        (parent-son pair, brothers pair, uncle-nephew pair)'''
        assert mode in MODES, f"Mode {mode} is unknown, allowed modes are {MODES}"
        assert train_test_split_type in TRAIN_TEST_SPLIT_TYPES, f"Split type {train_test_split_type} is unknown, allowed split type are {TRAIN_TEST_SPLIT_TYPES}"
        assert triples_type in TRIPLE_TYPES, f"triples type {triples_type} is unknown, allowed triples type are {TRIPLE_TYPES}"
        
        # load the tree or create it if it does not already exist
        if os.path.isfile(f"trees_files/{subreddit_name}.tree"):
            with open(f"trees_files/{subreddit_name}.tree", 'rb') as file:
                tree = pickle.load(file)
        else:
            tree = json_to_tree(subreddit_name)

        tree = self.split_dataset(tree, mode, train_test_split_type)
        all_samples = self.generate_all_triples(tree, triples_type, num_triples)

        print(f"Actual number of triples: {len(all_samples)}")
            
        return [((l[0], l[1]), (l[0], l[2]), (l[0], l[3])) for l in all_samples]
    
    def split_dataset(self, tree, mode, train_test_split_type):
        print(f"Splitting the dataset [{mode}]")
        # split according to mode (train: 4/5; test: 1/5)
        if train_test_split_type == 'thread': # we take a portion of the threads
            threads = tree.children("root")
            threads.sort(key = lambda n: n.data["num_children"], reverse = True)
            if mode == 'train':
                threads_to_remove = threads[4:5]
            else: # 'test'
                threads_to_remove = [n for i,n in enumerate(threads) if (i+1)%5]
            # Remove the extra threads
            [tree.remove_node(n.identifier) for n in threads_to_remove]
        else: # 'vertical': just like 'thread' but we keep a portion of each thread (so of the first messages)
            for thread in tree.children("root"):
                direct_answers = tree.children(thread.identifier)
                direct_answers.sort(key = lambda n: n.data["num_children"], reverse = True)
                if mode == 'train':
                    direct_answers_to_remove = direct_answers[4:5]
                else: # 'test'
                    direct_answers_to_remove = [n for i,n in enumerate(direct_answers) if (i+1)%5]
                # Remove the extra direct_answers
                [tree.remove_node(n.identifier) for n in direct_answers_to_remove]
        return tree
    
        
    def generate_all_triples(self, tree, triples_type, num_triples):
        print("Creating the triples.")
        # Build all triples
        ## We keep only the posts for which there exists a son, a brother and an uncle
        possible_base_nodes = list(tree.filter_nodes(lambda n: n.identifier != 'root' and  n.data["full_triple"]))
        ## If there are less possible_base_ids than we want triples (num_triples), we need to use 
        ## some posts several times as base posts in the 'unique' setting
        repeat_nodes = triples_type == 'unique' and num_triples > len(possible_base_nodes)
        if triples_type == "unique":
            all_nodes = np.random.choice(possible_base_nodes, num_triples, replace = repeat_nodes)
        else: # "rd"
            all_nodes = np.random.choice(possible_base_nodes, min(num_triples, len(possible_base_nodes)), replace = False)

        all_samples = []
        for node in tqdm(all_nodes, total=len(all_nodes), desc='Creating triples...'):
            node_id = node.identifier
            sons = get_sons(node_id, tree)
            brothers = get_brothers(node_id, tree)
            nephews = get_nephews(node_id, tree)

            base_node_text = node.data["text"]

            if len(sons) and len(brothers) and len(nephews):
                if triples_type == 'unique': # select one son, brother and nephew only
                    son = rd.choice(sons)
                    brother = rd.choice(brothers)
                    nephew = rd.choice(nephews)
                    all_samples.append([base_node_text, son.data["text"], brother.data["text"], nephew.data["text"]])
                else: # all: generate all triples
                    all_parent_pairs = [(base_node_text, n.data["text"]) for n in sons]
                    all_brother_pairs = [(base_node_text, n.data["text"]) for n in brothers]
                    all_nephew_pairs = [(base_node_text, n.data["text"]) for n in nephews]
            
                    # if all_parent_pairs and all_brother_pairs and all_nephew_pairs:
                    all_samples += [[p[0][0], p[0][1], p[1][1], p[2][1]] for p in itertools.product(all_parent_pairs, all_brother_pairs, all_nephew_pairs)]
                    if len(all_samples) >= num_triples:
                        all_samples = rd.choices(all_samples, k=num_triples)
                        return all_samples
        
        return all_samples
    
if __name__ == "__main__":
    args = parse_args()
    subreddit_name = args.subreddit_name
    mode = args.mode
    train_test_split_type = args.train_test_split_type
    triples_type = args.triples_type
    num_triples = args.num_triples

    RedditDataset(subreddit_name, mode, train_test_split_type, triples_type, num_triples, None)