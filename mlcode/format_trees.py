import json
from treelib import Tree
from tqdm import tqdm
import pickle

def has_full_triple(node_id, tree):
    if not tree.children(node_id): # no sons
        return False
    else: # there are sons
        brothers = tree.siblings(node_id)
        if not brothers: # no brothers
            return False
        else: # there are sons and brothers
            for bro in brothers:
                if tree.children(bro.identifier): # there are sons, brothers and nephews
                    return True
    return False # there are sons and brothers but no nephews
    

def json_to_tree(subreddit_name: str):
    with open(f"json_files/{subreddit_name}.json", 'r') as openfile:
        # Reading from json file
        all_posts = json.load(openfile)

    tree = Tree()

    # Create the root
    print(f"Subreddit [{subreddit_name}]")
    tree.create_node(tag = subreddit_name, identifier = "root")

    # Iterate through the posts
    for post in tqdm(all_posts, total=len(all_posts), desc='\tGoing through posts...'):
        post_data = {
            "id": post["id"],
            "url": post["url"],
            "text": '\n\n'.join([post["title"], post["text"]]).strip(),
            "upvote": post["upvote"],
            "author": post["author"],
            "num_children": 0,
            "depth": 0,
            "full_triple": False
        }
        tree.create_node(tag = post["id"], identifier = post["id"], data = post_data, parent="root")

        for child in post["children"]:
            parent = tree.get_node(child["parent_id"][3:])
            child_data = {
                "id": child["id"],
                "parent_id": child["parent_id"][3:],
                "text": child["text"],
                "upvote": child["upvote"],
                "author": child["author"],
                "num_children": 0,
                "depth": parent.data["depth"] + 1,
                "full_triple": False
            }
            tree.create_node(tag = child["id"], identifier = child["id"], data = child_data, parent=parent.identifier)
            parent.data["num_children"] += 1
    
    for node in tree.all_nodes():
        if node.identifier != "root" and node.data["depth"]:
            node.data["full_triple"] = has_full_triple(node.identifier, tree)
    
    # Save as a pickle file for easy loading
    with open(f"trees_files/{subreddit_name}.tree", "wb") as outfile:
        pickle.dump(tree, outfile)

    # Save as a json file for readability
    json_object = json.dumps(tree.to_dict(with_data=True), indent=4)
    # Writing to sample.json
    with open(f"trees_files/{subreddit_name}Tree.json", "w") as outfile:
        outfile.write(json_object)
        
    print(f"Subreddit {subreddit_name} done.")

    return tree
