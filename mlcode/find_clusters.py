import argparse
import pickle
from model import CoherenceNet
from transformers import AutoModel, AutoTokenizer
import torch
import pandas as pd
import os

THRESHOLD = .1

def parse_args():
    parser = argparse.ArgumentParser(usage='python find_clusters.py -s OnePiece -x 15000M2')
    parser.add_argument('-s', '--subreddit', help='name of the subreddit', type=str, default='AITAH')
    parser.add_argument('-p', '--model_path', help='path to where the model is saved', type=str, default='cpt_final.pth')
    parser.add_argument('-x', '--xp_name', help='name of the experiment', type=str, default='15000M2')
    args = parser.parse_args()
    return args

def escape_string(string: str):
    escaped_string = string.replace('\\', '\\\\') \
        .replace('"', '\\"') \
        .replace('\n', '\\n') \
        .replace('\r', '\\r') \
        .replace('\t', '\\t') \
        .replace('\b', '\\b') \
        .replace('\f', '\\f')
    return escaped_string

def similarity(head_comment, foot_comment, tokenizer, text_encoder):
    head_comment = escape_string(head_comment)
    foot_comment = escape_string(foot_comment)
    pair = tokenizer(head_comment, foot_comment, padding='max_length', max_length = 128, truncation=True, return_tensors='pt')
    output = text_encoder([[pair, pair, pair]])
    sim = output.squeeze().tolist()[0]
    return sim

def find_clusters(tree, tokenizer, text_encoder):
    data = []
    def fill_data(thread):
        head_comment = tree[thread[0]].data["text"]
        foot_comment = tree[thread[-1]].data["text"]
        sim = similarity(head_comment, foot_comment, tokenizer, text_encoder)
        data.append([thread[0], thread[-1], len(thread), ";".join(thread), sim])
    def cluster(node, thread):
        nid = node.identifier
        comment = node.data["text"]
        all_children = tree.children(nid)
        if all_children:
            children_sim = [similarity(comment, c.data["text"], tokenizer, text_encoder) for c in all_children]
            if max(children_sim) < THRESHOLD:
                fill_data(thread)
            for c, dis in zip(all_children, children_sim):
                if dis < THRESHOLD:
                    cluster(c, [c.identifier])
                else:
                    cluster(c, thread + [c.identifier])
        else:
            fill_data(thread)
        return data

    for c in tree.children('root'):
        data = cluster(c, [c.identifier])

    return pd.DataFrame(data, columns = ["head", "foot", "length", "commIds", "endsSim"])

def compute_similarities(subreddit: str, xp_name: str, model_path: str):
    # cpu or gpu
    if torch.cuda.is_available(): device = 'cuda'
    elif  torch.backends.mps.is_available(): device = 'mps'
    else: device = 'cpu'

    if not os.path.exists(f"./clusters"):
        os.makedirs(f"./clusters") 
    
    model_path = f"./checkpoints/{subreddit}/{xp_name}/{model_path}"

    with open(f"trees_files/boardgames.tree", 'rb') as file:
        tree = pickle.load(file)
    
    # for c in tree.children('root')[1:]:
    #     tree.remove_node(c.identifier)
    # for nid in tree.children('root'):
    #     for c in tree.children(nid.identifier)[2:]:
    #         tree.remove_node(c.identifier)
    # print(tree)
    
    text_encoder = CoherenceNet(AutoModel.from_pretrained('aws-ai/dse-bert-base'), device)
    if xp_name != 'None':
        checkpoint = torch.load(model_path, map_location=device)
        text_encoder.load_state_dict(checkpoint, strict=False)
    text_encoder.to(device)
    tokenizer = AutoTokenizer.from_pretrained('aws-ai/dse-bert-base')

    df_clusters = find_clusters(tree, tokenizer, text_encoder)
    df_clusters.to_csv(f"./clusters/{subreddit}{xp_name}_clusters.csv", sep='\t')
    return df_clusters

if __name__ == "__main__":
    args = parse_args()
    args = vars(args)
    subreddit = args['subreddit']
    xp_name = args['xp_name']
    model_path = args['model_path']

    compute_similarities(subreddit, xp_name, model_path)