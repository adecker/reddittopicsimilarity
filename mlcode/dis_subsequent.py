import argparse
import pickle
import graphviz
from graphviz import Source
from model import CoherenceNet
from transformers import AutoModel, AutoTokenizer
import torch
import matplotlib.cm as cm
from matplotlib.colors import rgb2hex
import pandas as pd
import os

import codecs
from io import StringIO

def parse_args():
    parser = argparse.ArgumentParser(usage='python test.py -s OnePiece -x 7500M2')
    parser.add_argument('-s', '--subreddit', help='path to where the model is saved', type=str, default='AITAH')
    parser.add_argument('-p', '--model_path', help='path to where the model is saved', type=str, default='cpt_final.pth')
    parser.add_argument('-x', '--xp_name', help='path to where the model is saved', type=str, default='7500M2')
    args = parser.parse_args()
    return args

def tree_to_graphviz(nodes, connections, filename=None):
    """Compute the distance from each message to the very first post and
    exports the subtree in the dot format of the graphviz software with colours
    based on the distance score."""

    # write nodes and connections to dot format
    is_plain_file = filename is not None
    if is_plain_file:
        f = codecs.open(filename, 'w', 'utf-8')
    else:
        f = StringIO()

    f.write('digraph subtree { \n')
    f.write('\tranksep=3\n')
    for n in nodes:
        f.write('\t' + n + '\n')

    if len(connections) > 0:
        f.write('\n')

    for c in connections:
        f.write('\t' + c + '\n')

    f.write('}')

    if not is_plain_file:
        print(f.getvalue())

    f.close()

def escape_string(string: str):
    escaped_string = string.replace('\\', '\\\\').replace('"', '\\"').replace('\n', '\\n').replace('\r', '\\r').replace('\t', '\\t').replace('\b', '\\b').replace('\f', '\\f')
    return escaped_string

def compute_distances(subreddit: str, xp_name: str, model_path: str, view: bool = False):
    # cpu or gpu
    if torch.cuda.is_available(): device = 'cuda'
    elif  torch.backends.mps.is_available(): device = 'mps'
    else: device = 'cpu'

    if not os.path.exists(f"./distance_subsequent/{subreddit}{xp_name}"):
        os.makedirs(f"./distance_subsequent/{subreddit}{xp_name}") 
    
    model_path = f".checkpoints/{subreddit}/{xp_name}/{model_path}"

    with open(f"trees_files/boardgames.tree", 'rb') as file:
        tree = pickle.load(file)

    text_encoder = CoherenceNet(AutoModel.from_pretrained('aws-ai/dse-bert-base'), device)
    if xp_name != 'None':
        checkpoint = torch.load(model_path, map_location=device)
        text_encoder.load_state_dict(checkpoint, strict=False)
    text_encoder.to(device)
    tokenizer = AutoTokenizer.from_pretrained('aws-ai/dse-bert-base')

    dict_distances = {}

    for main_post_node in tree.children('root'):
        main_id = main_post_node.identifier
        nodes, connections, distances = distance(tree.subtree(main_id), text_encoder, tokenizer)
        
        dict_distances[main_id] = distances
        tree_to_graphviz(nodes, connections, f"./distance_subsequent/{subreddit}{xp_name}/{subreddit}{xp_name}_{main_id}.gv")
        if view:
            display_graphviz(f"./distance_subsequent/{subreddit}{xp_name}/{subreddit}{xp_name}_{main_id}.gv")
        print(f"Subtree {main_id} done.")
        exit()

    max_len = max([len(l) for l in dict_distances.values()])
    for k, v in dict_distances.items():
        dict_distances[k] = v + [-1]*(max_len-len(dict_distances[k]))
    
    df_distances = pd.DataFrame(dict_distances)
    df_distances.to_csv(f"./distance_subsequent/{subreddit}{xp_name}_distances.csv", sep=';')
    return df_distances

def distance(subtree, text_encoder, tokenizer):
    distances = []
    nodes, connections = [], []
    main_id = subtree.root

    # TODO: Make it a param or set at 128
    max_length = 128

    for nid in subtree.expand_tree(main_id, mode=subtree.WIDTH):
        if nid == main_id:
            colour = "#FFFFFF"
        else:
            # retrieve pair of comments
            comment = escape_string(subtree[nid].data["text"])
            parent = subtree.parent(nid)
            parent_comment = escape_string(parent.data["text"])
            # compute distance
            text_encoder.eval()
            pair = tokenizer(parent_comment, comment, padding='max_length', max_length = max_length, truncation=True, return_tensors='pt')
            output = text_encoder([[pair, pair, pair]])
            val = output.squeeze().tolist()[0]
            colour = rgb2hex(cm.PiYG(val), keep_alpha=True)
            distances.append(val)
        
            # add to visualisation unless it is a thread of length < 2
            if subtree.children(nid) or subtree[nid].data["depth"] > 1:

                state = '"{0}" [label="{1}", shape=box, style=filled, fillcolor="{2}"]'.format(
                    nid, nid, colour)
                nodes.append(state)

                # add edge between parent and current node
                pid = subtree.parent(nid).identifier
                connections.append('"{0}" -> "{1}"'.format(pid, nid))
    return nodes, connections, distances

def display_graphviz(filepath):
    s = Source.from_file(filepath)
    s.render(format='jpg',view=True)
    

if __name__ == "__main__":
    args = parse_args()
    args = vars(args)
    subreddit = args['subreddit']
    xp_name = args['xp_name']
    model_path = args['model_path']

    compute_distances(subreddit, xp_name, model_path, view=False)