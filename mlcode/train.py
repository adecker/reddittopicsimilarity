import argparse
import torch
from torch.utils.data import DataLoader, random_split
from transformers import AdamW
from transformers import get_linear_schedule_with_warmup
from data_trees import RedditDataset
from model import CoherenceNet
from test import test
from transformers import AutoModel, AutoTokenizer
from tqdm import tqdm
import os

import warnings
warnings.filterwarnings('ignore', category=UserWarning, message='TypedStorage is deprecated')


def parse_args():
    parser = argparse.ArgumentParser(usage='python train.py -e 5 -s boardgames -n 10000')
    parser.add_argument('-e', '--epochs', help='number of training epochs', type=int, default=5)
    parser.add_argument('-b', '--batch_size', help='batch size', type=int, default=16)
    parser.add_argument('-t', '--text_encoder', help='text encoder for utterances', type=str, default='aws-ai/dse-bert-base')
    parser.add_argument('-s', '--subreddit_name', help='name of the used subreddit', type=str, default='boardgames')
    parser.add_argument('-n', '--num_triples', help='number of triples in the dataset', type=int, default=100000)
    parser.add_argument('-m', '--mode', help='train or test mode', type=str, default='train')
    parser.add_argument('-i', '--train_test_split_type', help='spliting method for the train/test split', type=str, default='thread')
    parser.add_argument('-j', '--triplets_type', help='all triplets or one per comment', type=str, default='unique')
    parser.add_argument('-c', '--checkpoints_path', help='path to save checkpoints', type=str, default='./checkpoints/')
    parser.add_argument('-x', '--xp_id', help='experiment identifier', type=str, default='test')
    args = parser.parse_args()
    return args
    # "vertical", "all", "rd_comments", 250, tokenizer

def collate_fn(batch):
    return batch

def marginal_ranking_loss(batch, margin):
    # Calculate pairwise differences
    batch_tensor = batch[:, :, :]

    # Compute individual losses
    # coh1 > coh2, coh1 > coh3, coh2 > coh3
    loss_coh1_coh2 = torch.nn.functional.relu(margin - (batch_tensor[:, 0] - batch_tensor[:, 1]))
    loss_coh1_coh3 = torch.nn.functional.relu(margin - (batch_tensor[:, 0] - batch_tensor[:, 2]))
    loss_coh2_coh3 = torch.nn.functional.relu(margin - (batch_tensor[:, 1] - batch_tensor[:, 2]))

    # Sum and average the losses
    total_loss = (loss_coh1_coh2 + loss_coh1_coh3 + loss_coh2_coh3) / 3
    total_loss = torch.mean(total_loss)

    return total_loss

def validation_metric(sample_list):
    # one triple break down into 3 pairs: a>b, a>c, b>c
    count = 0
    for sample in sample_list:
        [a, b, c] = sample
        pairs = [(a, b), (a, c), (b, c)]
        count += sum(1 for x, y in pairs if x > y)

    return count/float(len(sample_list)*3)


def train(model, train_dataloader, val_dataloader, optimizer, epochs, checkpoints_path, margin=1, test_args={}):
    # Total number of training steps is number of batches * number of epochs.
    total_steps = len(train_dataloader) * epochs
    # Create the learning rate scheduler.
    scheduler = get_linear_schedule_with_warmup(optimizer, num_warmup_steps = 0, num_training_steps = total_steps)

    val_res = validation(model, val_dataloader)  # apply model from this step to the validation set
    val_log = f'Validation Results - Before training: {val_res}\n'
    print(val_log)
    validation_results = [val_res]
    losses = []
    evaluation_results = []

    # eval_epochs = list(range(epochs-7, epochs+1))

    for epoch_i in range(0, epochs):
        print("")
        print('======== Epoch {:} / {:} ========'.format(epoch_i + 1, epochs))

        total_loss = 0
        model.train()

        for batch in tqdm(train_dataloader, total=len(train_dataloader), desc='Training...'):

            model.zero_grad()
            output = model(batch)  # prediction

            loss = marginal_ranking_loss(output, margin) # loss computing
            total_loss += loss.item()

            loss.backward()  # back-propagte
            torch.nn.utils.clip_grad_norm_(model.parameters(), 1.0)
            optimizer.step()
            scheduler.step()

        # # Save model
        val_res = validation(model, val_dataloader)  # apply model from this step to the validation set
        val_log = f'\tValidation Results: {val_res}'
        print(val_log)
        if epoch_i+1 == 3:
            print(f"\tModel saved [{epoch_i + 1}].")
            # print("==== Eval ====")
            torch.save(model.state_dict(), f"{checkpoints_path}/e{epoch_i+1}.pth")  # save the model checkpoint for this step.
            # test_args['model_path'] = f"e{epoch_i+1}.pth"
            # result = test(**test_args)
            # evaluation_results.append(result)
            # print("==============")
        validation_results.append(val_res)
 
        avg_train_loss = total_loss / len(train_dataloader)#len(train_dataloader)  # mean loss for the epoch
        epoch_log = f'\n=========== The loss for epoch {epoch_i + 1} is: {avg_train_loss}\n'
        print(epoch_log)
        losses.append(avg_train_loss)
    # print(f"Eval for epochs {eval_epochs}: {evaluation_results}.")
    print(f"Losses: {losses}.")
    print(f"Validation: {validation_results}.")

def validation(model, val_dataloader):
    coherence_scores = []
    model.eval()
    with torch.no_grad():
        for step, val_batch in tqdm(enumerate(val_dataloader), total=len(val_dataloader), desc='Validating...'):
            output = model(val_batch)
            coherence_scores += output.tolist()
    metric_res = validation_metric(coherence_scores)
    return metric_res

def main():
    # Load settings
    args = parse_args()
    epochs = args.epochs
    batch_size = args.batch_size
    checkpoints_path = f"{args.checkpoints_path}{args.subreddit_name}/{args.xp_id}/"
    subreddit_name = args.subreddit_name
    mode = args.mode
    train_test_split_type = args.train_test_split_type
    triplets_type = args.triplets_type
    num_triples = args.num_triples

    test_args = {
     'batch_size': batch_size,
     'subreddit': subreddit_name,
     'xp_name': args.xp_id,
     'text_encoder': args.text_encoder,
     'num_triples': 10000,
     'model_path': ''
    }

    # Create the folder to save the model if needed
    if not os.path.exists(checkpoints_path):
        os.makedirs(checkpoints_path)

    # Print the settings for keeping a trace
    print(f"[{subreddit_name}] {epochs} epochs, {num_triples} triples in batches of {batch_size}")
    print(f"\t{mode}; {train_test_split_type}; {triplets_type}")
    
    # cpu or gpu
    if torch.cuda.is_available(): device = 'cuda'
    elif  torch.backends.mps.is_available(): device = 'mps'
    else: device = 'cpu'

    # Load encoder model
    model = AutoModel.from_pretrained(args.text_encoder).to(device)
    tokenizer = AutoTokenizer.from_pretrained(args.text_encoder)

    # Create dataset and dataloader
    # subreddit_name, mode, train_test_split_type, triplets_type, max_triplets, num_triples, tokenizer, max_length=128
    print("Loading dataset...")
    full_dataset = RedditDataset(subreddit_name, mode, train_test_split_type, triplets_type, num_triples, tokenizer)
    # Determine sizes for train and validation sets
    val_size = int(0.1 * len(full_dataset))  # for 10% validation set
    train_size = len(full_dataset) - val_size
    #train_dataset = RedditDataset(subreddit_name, mode, train_test_split_type, triplets_type, num_triples, tokenizer)

    # Split the dataset
    train_dataset, val_dataset = random_split(full_dataset, [train_size, val_size])
    train_dataloader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True, collate_fn=collate_fn)
    val_dataloader = DataLoader(val_dataset, batch_size=batch_size, shuffle=True, collate_fn=collate_fn)

    # Utterance-pair coherence scoring model
    model = CoherenceNet(model, device)
    model.to(device)
    optimizer = AdamW(model.parameters(), lr = 2e-5, eps = 1e-8)
    # optimizer = torch.optim.AdamW(model.parameters(), lr = 2e-5, eps = 1e-8)

    # Training loop
    print("Training model...")
    train(model, train_dataloader, val_dataloader, optimizer, epochs, checkpoints_path, test_args=test_args)

    # Save final model
    torch.save(model.state_dict(), f"{checkpoints_path}/cpt_final.pth")  # save the model checkpoint for this step.


if __name__ == "__main__":
    main()
  