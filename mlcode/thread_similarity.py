import argparse
import pickle
from model import CoherenceNet
from transformers import AutoModel, AutoTokenizer
import torch
import pandas as pd
import os
import matplotlib.pyplot as plt
from tqdm import tqdm

THRESHOLD = .1
SWITCH_THRESHOLD = .7

def parse_args():
    parser = argparse.ArgumentParser(usage='python find_clusters.py -s OnePiece -x 15000M2')
    parser.add_argument('-s', '--subreddit', help='path to where the model is saved', type=str, default='Coronavirus')
    parser.add_argument('-p', '--model_path', help='path to where the model is saved', type=str, default='cpt_final.pth')
    parser.add_argument('-x', '--xp_name', help='path to where the model is saved', type=str, default='15000M2c')
    args = parser.parse_args()
    return args

def escape_string(string: str):
    escaped_string = string.replace('\\', '\\\\') \
        .replace('"', '\\"') \
        .replace('\n', '\\n') \
        .replace('\r', '\\r') \
        .replace('\t', '\\t') \
        .replace('\b', '\\b') \
        .replace('\f', '\\f')
    return escaped_string

def similarity(head_comment, foot_comment, tokenizer, text_encoder):
    head_comment = escape_string(head_comment)
    foot_comment = escape_string(foot_comment)
    pair = tokenizer(head_comment, foot_comment, padding='max_length', max_length = 128, truncation=True, return_tensors='pt')
    output = text_encoder([[pair, pair, pair]])
    sim = output.squeeze().tolist()[0]
    return sim

def compute_thread_sims(tree, tokenizer, text_encoder):
    all_threads = []
    all_stats_threads = []
    all_main_msgs = []
    all_posts = [c.identifier for c in tree.children('root')]
    all_main_messages = [(c.identifier, c.data["text"]) for c in tree.children('root')]

    all_leaves = [(tree.depth(current_node), current_node, msg[1]) for msg, p in zip(all_main_messages, all_posts) for current_node in tree.leaves(p)]
    all_leaves.sort(key = lambda e: e[0], reverse=True)
    all_leaves = [x for x in all_leaves if x[0] > 9]

    for d,l,main_msg in tqdm(all_leaves, total=len(all_leaves), desc="Computing clusters..."):
        cluster_heads = [] # The clusters for a subthread
        current_nid = l.identifier
        current_depth = d
        stats_thread = {
            'leaf': l.identifier,
            'nb_msg': current_depth,
            'nb_threads': 0,
            'len_final_digression': 0,
            'depth_final_digression': 0,
            'nb_switch': 0,
            'len_on_topic_clusters': [],
            'sim_beginning': -1,
            'sim_end': -1
        }
        on_final_digression = True
        # The stats: nb msg, nb threads, len plateau final < .1, depth appartition plateau, 
        # nb basculements, taille clusters proches 1, sim début, sim fin
        foot_comment = l.data["text"]
        while current_nid not in all_posts:
            parent = tree.parent(current_nid)
            head_comment = parent.data["text"]
            sim = similarity(head_comment, foot_comment, tokenizer, text_encoder)
            if sim < THRESHOLD: 
                root_sim = similarity(main_msg, foot_comment, tokenizer, text_encoder)
                if not len(cluster_heads): # add end of the thread
                    cluster_heads.append((l.identifier, d, root_sim, "<END>"))
                    stats_thread['sim_end'] = root_sim
                else:
                    # check if switch
                    if abs(cluster_heads[-1][-2] - root_sim) > SWITCH_THRESHOLD:
                        stats_thread['nb_switch'] += 1
                cluster_heads.append((current_nid, current_depth, root_sim, foot_comment))
                # check if on topic cluster
                if root_sim > 1 - THRESHOLD:
                    stats_thread['len_on_topic_clusters'].append(cluster_heads[-1][1] - current_depth)
                if on_final_digression:
                    if root_sim < THRESHOLD: # still on final digression
                        stats_thread['depth_final_digression'] = current_depth
                        stats_thread['len_final_digression'] = d - current_depth
                    else:
                        on_final_digression = False
                stats_thread['nb_threads'] += 1
                
            current_nid = parent.identifier
            current_depth -= 1
            foot_comment = head_comment
        if sim >= THRESHOLD:
            cluster_heads.append((current_nid, current_depth, sim, foot_comment))
            # necessarily not a switch because sim >= THRESHOLD
            # necessarily on topic cluster because sim >= THRESHOLD
            stats_thread['len_on_topic_clusters'].append(cluster_heads[-1][1] - current_depth)
            # cannot be final digression since sim >= THRESHOLD
            stats_thread['nb_threads'] += 1
        stats_thread['sim_beginning'] = sim
        all_threads.append(cluster_heads)
        all_stats_threads.append(stats_thread)
        all_main_msgs.append(main_msg)
    # print(all_threads[0])
    # print(all_stats_threads[0])
    # print(all_main_msgs[0])
    return all_threads, all_stats_threads, all_main_msgs


def compute_similarities(subreddit: str, xp_name: str, model_path: str):
    # cpu or gpu
    if torch.cuda.is_available(): device = 'cuda'
    elif  torch.backends.mps.is_available(): device = 'mps'
    else: device = 'cpu'

    if not os.path.exists(f"./clusters"):
        os.makedirs(f"./clusters") 
    
    model_path = f"/Users/Amandine/Desktop/Checkpoints M2 Thread/{subreddit}/{xp_name}/{model_path}"

    with open(f"trees_files/{subreddit}.tree", 'rb') as file:
        tree = pickle.load(file)
    
    # for c in tree.children('root')[1:]:
    #     tree.remove_node(c.identifier)
    # for nid in tree.children('root'):
    #     for c in tree.children(nid.identifier)[2:]:
    #         tree.remove_node(c.identifier)
    # print(tree)
    
    text_encoder = CoherenceNet(AutoModel.from_pretrained('aws-ai/dse-bert-base'), device)
    if xp_name != 'None':
        print("Load")
        checkpoint = torch.load(model_path, map_location=device)
        text_encoder.load_state_dict(checkpoint, strict=False)
    text_encoder.to(device)
    tokenizer = AutoTokenizer.from_pretrained('aws-ai/dse-bert-base')
    text_encoder.eval()

    all_threads, all_stats_threads, all_main_msgs = compute_thread_sims(tree, tokenizer, text_encoder)
    all_res = list(zip(all_threads, all_stats_threads, all_main_msgs))
    
    all_res.sort(key = lambda l: sum([x[-2]>.5 and x[-2]<.9 for x in l[0]]), reverse=True)
    all_threads = [x for x,y,z in all_res]
    all_stats_threads = [y for x,y,z in all_res[::-1]]
    df_stats = pd.DataFrame(all_stats_threads)
    df_stats.to_csv(f"stats/{subreddit}OneLayer10.csv", sep=';')
    all_main_msgs = [z for x,y,z in all_res]
    
    for i, l in enumerate(all_threads):
        with open(f"clusters/{subreddit}_{l[0][0]}Msgs.txt", 'w') as f: 
            X, Y, msgs = [x[1] for x in l[::-1]], [y[2] for y in l[::-1]], [m[-1] for m in l[::-1]]
            f.write(f"[{l[0][0]}]\n")
            f.write(f"{all_main_msgs[i]}\n")
            # print(all_stats_threads[i])
            # print(all_main_msgs[i])
            # print("********************************")
            # for t in l[::-1]:
            #     print(f"\t{t[-1]}")
            #     print("\t********************************")
            # print("********************************")
            for j, y in enumerate(Y[:-1]):
                plt.hlines(y, xmin = X[j], xmax = X[j+1], colors=['darkslateblue'], linewidth=1.5)
                f.write(f"[{j}] {msgs[j]}\n")
                plt.plot(X[j], y, '|', alpha=1, c='darkslateblue')
            plt.title(f"Subreddit {subreddit} [{l[0][0]}]")
            plt.xlabel("Comment depth")
            plt.ylabel("Similarity to the main post")
            # plt.show()
            plt.savefig(f"clusters/{subreddit}_{l[0][0]}Img.png")
            plt.clf()
            f.write("")

        # print("'\n\n##########################################\n\n")
    

    # plt.savefig(f"sim_threads/{subreddit}_mid.png")
    plt.clf()

    # return many_clusters, long_threads

if __name__ == "__main__":
    args = parse_args()
    args = vars(args)
    subreddit = args['subreddit']
    xp_name = args['xp_name']
    model_path = args['model_path']

    subreddits = ["Coronavirus"]#, "Art", "gaming", "OnePiece", "sports"]
    for subreddit in subreddits:
        compute_similarities(subreddit, xp_name, model_path)