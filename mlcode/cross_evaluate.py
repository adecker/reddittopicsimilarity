import argparse
import torch
from tqdm import tqdm
from torch.utils.data import DataLoader
from data_trees import RedditDataset
from model import CoherenceNet
from transformers import AutoModel, AutoTokenizer, logging
logging.set_verbosity_warning()
from csv import DictWriter

import warnings
warnings.filterwarnings('ignore', category=UserWarning, message='TypedStorage is deprecated')

def parse_args():
    parser = argparse.ArgumentParser(usage='python cross_evaluate.py -d AITAH -m OnePiece -x test')
    parser.add_argument('-b', '--batch_size', help='batch size', type=int, default=16)
    parser.add_argument('-e', '--text_encoder', help='text encoder for utterances', type=str, default='aws-ai/dse-bert-base')
    parser.add_argument('-d', '--data_subreddit', help='path to where the model is saved', type=str, default='AITAH')
    parser.add_argument('-m', '--model_subreddit', help='path to where the model is saved', type=str, default='OnePiece')
    parser.add_argument('-p', '--model_path', help='path to where the model is saved', type=str, default='cpt_final.pth')
    parser.add_argument('-x', '--xp_name', help='path to where the model is saved', type=str, default='test')
    parser.add_argument('-n', '--num_triples', help='number of test example', type=int, default=100000)
    parser.add_argument('-i', '--train_test_split_type', help='spliting method for the train/test split', type=str, default='vertical')
    parser.add_argument('-j', '--triplets_type', help='all triplets or one per comment', type=str, default='unique')
    args = parser.parse_args()
    return args

def collate_fn(batch):
    return batch

def evaluation_metric(sample_list) -> float:
    # one triple break down into 3 pairs: a>b, a>c, b>c
    count = 0
    for sample in sample_list:
        [a, b, c] = sample
        pairs = [(a, b), (a, c), (b, c)]
        count += sum(1 for x, y in pairs if x > y)

    return count/float(len(sample_list)*3)

def evaluation(model, val_dataloader) -> float:
    coherence_scores = []
    model.eval()
    with torch.no_grad():
        for val_batch in tqdm(val_dataloader, total=len(val_dataloader), desc='Evaluating...'):
            output = model(val_batch)
            coherence_scores += output.tolist()
    metric_res = evaluation_metric(coherence_scores)
    return metric_res


def eval(verbose: bool = True, **args) -> float:
    # # Load settings
    # args = parse_args()
    batch_size = args['batch_size']
    model_subreddit = args['model_subreddit']
    data_subreddit = args['data_subreddit']
    xp_name = args['xp_name']
    model_path = f"./checkpoints/{model_subreddit}/{xp_name}/{args['model_path']}"
    text_encoder = args['text_encoder']
    num_triples = int(args['num_triples'])
    train_test_split_type = args['train_test_split_type']
    triplets_type = args['triplets_type']

    mode = "test"

    if verbose:
        # Print the settings for keeping a trace
        print(f"[data {data_subreddit} -- model {model_subreddit} {xp_name}] {num_triples} triples in batches of {batch_size}")
        print(f"\t{mode}; {train_test_split_type}; {triplets_type}")

    # cpu or gpu
    if torch.cuda.is_available(): device = 'cuda'
    elif  torch.backends.mps.is_available(): device = 'mps'
    else: device = 'cpu'

    # Load encoder model
    #model = AutoModel.from_pretrained(args.text_encoder).to(device)
    text_encoder = CoherenceNet(AutoModel.from_pretrained(text_encoder), device)
    if xp_name != 'None':
        if verbose:
            print("Load model.")
        checkpoint = torch.load(model_path, map_location=torch.device(device))
        text_encoder.load_state_dict(checkpoint, strict=False)
    text_encoder.to(device)
    tokenizer = AutoTokenizer.from_pretrained(args['text_encoder'])

    # Load dataset
    test_dataset = RedditDataset(data_subreddit, 
                                 mode, 
                                 train_test_split_type, 
                                 triplets_type, 
                                 num_triples, 
                                 tokenizer)

    # Split the dataset
    test_dataloader = DataLoader(test_dataset, 
                                 batch_size=batch_size, 
                                 shuffle=False, 
                                 collate_fn=collate_fn)

    result = evaluation(text_encoder, test_dataloader)
    if verbose:
        print(result)
    return result

if __name__ == "__main__":
    args = vars(parse_args())
    eval(**args)

    subreddits = ['AITAH', 'Art', 'AskReddit', 'boardgames', 'Coronavirus', 'gaming', 'OnePiece', 'PoliticalDiscussion', 'sports', 'todayilearned']
    field_names = ['data V \ model >', 'AITAH', 'Art', 'AskReddit', 'boardgames', 'Coronavirus', 'gaming', 'OnePiece', 'PoliticalDiscussion', 'sports', 'todayilearned']

    results = {s: -1 for s in subreddits}
    results['data V \ model >'] = args['data_subreddit']

    for model in subreddits:
        args['model_subreddit'] = model
        results[model] = test(**args)

    with open("crosseval.csv", 'a', newline='') as f:
        dictwriter_object = DictWriter(f, fieldnames=field_names)
        dictwriter_object.writerow(results)
    
    print(f"Original data [{model}]\n\t{results}")