import argparse
import torch
from tqdm import tqdm
from torch.utils.data import DataLoader
from data_trees import RedditDataset
from model import CoherenceNet
from transformers import AutoModel, AutoTokenizer
import pandas as pd

import warnings
warnings.filterwarnings('ignore', category=UserWarning, message='TypedStorage is deprecated')

def parse_args():
    parser = argparse.ArgumentParser(usage='python test.py -s OnePiece -x test')
    parser.add_argument('-b', '--batch_size', help='batch size', type=int, default=16)
    parser.add_argument('-e', '--text_encoder', help='text encoder for utterances', type=str, default='aws-ai/dse-bert-base')
    parser.add_argument('-s', '--subreddit', help='path to where the model is saved', type=str, default='OnePiece')
    parser.add_argument('-p', '--model_path', help='path to where the model is saved', type=str, default='cpt_final.pth')
    parser.add_argument('-x', '--xp_name', help='path to where the model is saved', type=str, default='test')
    parser.add_argument('-n', '--num_triples', help='number of test example', type=int, default=100000)
    parser.add_argument('-i', '--train_test_split_type', help='spliting method for the train/test split', type=str, default='vertical')
    parser.add_argument('-j', '--triplets_type', help='all triplets or one per comment', type=str, default='unique')
    args = parser.parse_args()
    return args

def collate_fn(batch):
    return batch

def evaluation_metric(sample_list):
    # one triple break down into 3 pairs: a>b, a>c, b>c
    count = 0
    all_comparisons = []
    for sample in sample_list:
        [a, b, c] = sample
        pairs = [(a, b), (a, c), (b, c)]
        pairs_comparison = [1 if x > y else 0 for x, y in pairs]
        count += sum(pairs_comparison)
        all_comparisons.append(pairs_comparison)

    return count/float(len(sample_list)*3), all_comparisons

def evaluation(model, val_dataloader, device):
    coherence_scores = []
    model.eval()
    with torch.no_grad():
        for val_batch in tqdm(val_dataloader, total=len(val_dataloader), desc='Evaluating...'):
            output = model(val_batch)
            coherence_scores += output.tolist()
    metric_res, all_comp = evaluation_metric(coherence_scores)
    return metric_res, all_comp


def test(**args):
    # # Load settings
    # args = parse_args()
    batch_size = args['batch_size']
    subreddit = args['subreddit']
    xp_name = args['xp_name']
    model_path = f"./checkpoints/{subreddit}/{xp_name}/{args['model_path']}"
    text_encoder = args['text_encoder']
    num_triples = int(args['num_triples'])
    train_test_split_type = args['train_test_split_type']
    triplets_type = args['triplets_type']

    mode = "test"

    # Print the settings for keeping a trace
    print(f"[{subreddit} -- model {xp_name}] {num_triples} triples in batches of {batch_size}")
    print(f"\t{mode}; {train_test_split_type}; {triplets_type}")

    # cpu or gpu
    if torch.cuda.is_available(): device = 'cuda'
    elif  torch.backends.mps.is_available(): device = 'mps'
    else: device = 'cpu'

    # Load encoder model
    #model = AutoModel.from_pretrained(args.text_encoder).to(device)
    text_encoder = CoherenceNet(AutoModel.from_pretrained(text_encoder), device)
    if xp_name != 'None':
        print("Load model.")
        checkpoint = torch.load(model_path, map_location=torch.device(device))
        text_encoder.load_state_dict(checkpoint, strict=False)
    text_encoder.to(device)
    tokenizer = AutoTokenizer.from_pretrained(args['text_encoder'])

    # Load dataset
    test_dataset = RedditDataset(subreddit, 
                                 mode, 
                                 train_test_split_type, 
                                 triplets_type, 
                                 num_triples, 
                                 tokenizer)

    # Split the dataset
    test_dataloader = DataLoader(test_dataset, batch_size=batch_size, shuffle=False, collate_fn=collate_fn)

    result, comparisons = evaluation(text_encoder, test_dataloader, device)
    print(result)
    df_comp = pd.DataFrame(comparisons, columns=['parents > brothers', 'parents > nephews', 'brothers > nephews'])
    s = df_comp.sum()
    m = df_comp.mean()
    total = [f"{s}/{len(df_comp)} ({m:.0%})" for s,m in zip(s,m)]
    df_comp.loc['total'] = total
    print(df_comp.loc['total'])
    return result

if __name__ == "__main__":
    args = parse_args()
    test(**vars(args))